package com.artivisi.training.designpattern.builder;

public class Aplikasi {
    public static void main(String[] args) {

        Customer c = new Customer();
        //c.setName("Endy");
        c.setEmail("endy@gmail.com");
        c.setAddress("Bogor");

        Customer c1 = Customer
                .builder()
                .name("wahyu")
                .build();
    }
}
