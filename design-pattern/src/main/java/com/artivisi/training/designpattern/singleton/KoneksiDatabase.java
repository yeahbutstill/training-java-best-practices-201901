package com.artivisi.training.designpattern.singleton;

public class KoneksiDatabase {

    public static KoneksiDatabase buatKoneksi(){
        if(objKoneksi == null) {
            objKoneksi = new KoneksiDatabase();
        }
        return objKoneksi;
    }

    private static KoneksiDatabase objKoneksi;

    private KoneksiDatabase(){}

    public void connect(){}
    public void jalankanQuery(String sql){}
    public void disconnect(){}
}
