package com.artivisi.training.designpattern.command;

public class CommandDemo {
    public static void main(String[] args) {
        // tanpa command pattern
        // pakai facade pattern
        AplikasiService service = new AplikasiService();
        service.simpanProduk();
        service.simpanTransaksi();

        // pakai command pattern
        AplikasiServiceExecutor executor = new AplikasiServiceExecutor();
        executor.jalankan(new SimpanProdukCommand());
        executor.jalankan(new SimpanTransaksiCommand());
    }
}
