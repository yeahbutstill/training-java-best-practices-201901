package com.artivisi.training.designpattern.command;

public class SimpanTransaksiCommand implements Command {
    public void jalankan() {
        System.out.println("Menyimpan data transaksi");
    }
}
