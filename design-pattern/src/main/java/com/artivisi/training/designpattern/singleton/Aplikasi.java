package com.artivisi.training.designpattern.singleton;

public class Aplikasi {
    public static void main(String[] args) {

        /*
        KoneksiDatabase koneksi1 = new KoneksiDatabase();
        KoneksiDatabase koneksi2 = new KoneksiDatabase();
        KoneksiDatabase koneksi3 = new KoneksiDatabase();
        KoneksiDatabase koneksi4 = new KoneksiDatabase();
        */

        KoneksiDatabase koneksi1 = KoneksiDatabase.buatKoneksi();
        KoneksiDatabase koneksi2 = KoneksiDatabase.buatKoneksi();
        KoneksiDatabase koneksi3 = KoneksiDatabase.buatKoneksi();
        KoneksiDatabase koneksi4 = KoneksiDatabase.buatKoneksi();
    }
}
